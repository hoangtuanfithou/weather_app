//
//  Constants.swift
//  WeatherApp
//
//  Created by Nguyen Hoang Tuan on 2/9/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import UIKit
import Foundation

let searchHistoryKey = "SearchHistory"
let maxHistoryNumber = 10
let weatherUrl = "https://api.worldweatheronline.com/premium/v1/weather.ashx"
let requestKey = "c47d4376fb194239afc21345170910"

let appDelegate = UIApplication.shared.delegate as! AppDelegate // swiftlint:disable:this force_cast
let mainContext = appDelegate.persistentContainer.viewContext

typealias CmCallback = (Bool, [WeatherResponse]?) -> Void

enum DataUsing {
    case coreData, userDefault, realm, sqlite, cloudKit
}

var dataUsing = DataUsing.cloudKit
