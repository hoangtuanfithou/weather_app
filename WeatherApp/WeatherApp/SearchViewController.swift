//
//  SearchViewController.swift
//  WeatherApp
//
//  Created by Nguyen Hoang Tuan on 2/8/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    var histories: [WeatherResponse] = []
    
    @IBOutlet weak var historyTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var weatherPresenter: WeatherBusiness!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weatherPresenter = WeatherBusiness(view: self)
    }
    
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBar.isFirstResponder {
            let historiesCount = histories.count > maxHistoryNumber ? maxHistoryNumber : histories.count
            return historiesCount
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath)
        cell.textLabel?.text = histories[indexPath.row].city
        return cell
    }
    
    // MARK: UISearchBarDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ShowDetailView", sender: histories[indexPath.row])
    }
    
    // MARK: UISearchBarDelegate
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        histories = WeatherBusiness.getSearchHistories(callBack: { (_, weatherResponse) in // call back for using with CloudKit
            if let weatherResponse = weatherResponse {
                self.histories = weatherResponse
                DispatchQueue.main.sync {
                    self.historyTableView.reloadData()   
                }
            }
        })
        historyTableView.reloadData()
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let query = searchBar.text, !query.isEmpty else {
            return
        }
        weatherPresenter.searchWeather(query: query)
    }
    
    // MARK: - Navigation
    func searchWeatherSuccess(weatherResponse: WeatherResponse) {
        if weatherResponse.city != nil {
            WeatherBusiness.saveSearchHistory(weatherResponse: weatherResponse)
            performSegue(withIdentifier: "ShowDetailView", sender: weatherResponse)
        } else {
            showAlert(message: "Unable to find any matching weather location to the query submitted!")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetailView",
            let detailView = segue.destination as? DetailViewController,
            let weather = sender as? WeatherResponse {
            detailView.weather = weather
        }
    }
    
    private func showAlert(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension SearchViewController: WeatherProtocol {
    
    func getWeatherFinish(success: Bool, weather: WeatherResponse) {
        searchWeatherSuccess(weatherResponse: weather)
    }
    
}
